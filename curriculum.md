## curriculum vitae

![foto](/imagin/f1.png)

**Nombre:** *Lennon Pereira*

**Email:** *lennonpr08@gmail.com*

**Tel:** *661149414*

## IDIOMAS


| Idiomas | Escrita | Comprecion oral |
| ------ | ------ | ------ |
| Portugués | Perfecta | Perfecta |
| Castellano | Bien | Bien |
| Catalán | Bien | Bien |
| Inglés | Media | Media |

## Formacion

**Instituto Poblenou**

*GM Sistemas Microinformáticos y Redes (Cursando)*

**Instituto Secretari Coloma**

*Eso (2017)*

## COMPETENCIAS

*Cooperación*

*Autoconfianza*

*Planificación y Organización*

*Gestión del Estrés*

*Orientación al cliente*

*Compromiso con la organización*

*Trabajo en equipo.*